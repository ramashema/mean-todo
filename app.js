const express = require('express');
const logger = require('morgan');
const passport = require('passport');
const cors = require('cors');

const apiRouter = require('./api/routers/routers');
require('./api/models/db');
require('./api/passport/config');

const app = express();
app.set('port', 4000);

const corsOptions = {
    origin: `http://localhost:4200`,
    credentials: true,
    optionSuccessStatus: 200,
    exposedHeaders: 'Authorization'
}

app.use(logger('combined'));
app.use(cors(corsOptions));
app.use(express.urlencoded({extended: true}));
app.use(express.json());

app.use('/api',apiRouter);
app.use(passport.initialize());
app.use((request, response, next) => {
    return next(errorHandler(response));
});

app.use((error, request, response) => {
    return response.status(error.statusCode).json({
        name: error.name,
        message: error.statusMessage
    });
});
const errorHandler = (response) =>{
    return response.status(404).json({
        status: "error",
        message: "invalid url"
    })
}
//error handling middleware

app.listen(app.get('port'), () => {
    console.log(`express is running on port ${app.get('port')}`);
});