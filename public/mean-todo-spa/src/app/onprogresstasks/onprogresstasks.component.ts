import {Component, OnInit} from '@angular/core';
import {Task} from "../task";
import {TasksDataService} from "../tasks-data.service";
import {Router} from "@angular/router";
import {ChangeTaskStatusService} from "../change-task-status.service";
import {response} from "express";

@Component({
  selector: 'app-onprogresstasks',
  templateUrl: './onprogresstasks.component.html',
  styleUrls: ['./onprogresstasks.component.css']
})
export class OnprogresstasksComponent implements OnInit{
  public tasks?: Task[];
  public info?: string;
  public error?: string;
  public taskStatus: string = 'on progress'

  constructor(
    private taskDataService: TasksDataService,
    private router: Router,
    private changeTaskStatus: ChangeTaskStatusService) {}

  public getUserTasks(): void {
    this.taskDataService.getUserTasks()
      .then(receivedTasks => {
          this.tasks = receivedTasks.filter(task => task.status === 'on-progress');
      })
      .catch(error => {
          alert(error.statusText)
          return this.router.navigate(['/']);
      });
  }
  ngOnInit() {
    this.getUserTasks();
  }

  markAsCompleted(_id: string | undefined, $event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
    this.changeTaskStatus.updateTaskStatus(_id)
      .then( response => {
        this.tasks = this.tasks?.filter( task => task._id !== _id );
        this.info = response.message
      } ).catch(error => {
        this.error = error.message
    });
  }
}
