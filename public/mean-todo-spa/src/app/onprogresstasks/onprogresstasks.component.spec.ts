import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OnprogresstasksComponent } from './onprogresstasks.component';

describe('OnprogresstasksComponent', () => {
  let component: OnprogresstasksComponent;
  let fixture: ComponentFixture<OnprogresstasksComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OnprogresstasksComponent]
    });
    fixture = TestBed.createComponent(OnprogresstasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
