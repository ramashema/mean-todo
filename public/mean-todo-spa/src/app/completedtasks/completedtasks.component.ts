import {Component, OnInit} from '@angular/core';
import {Task} from "../task";
import {TasksDataService} from "../tasks-data.service";
import {Router} from "@angular/router";
import {response} from "express";

@Component({
  selector: 'app-completedtasks',
  templateUrl: './completedtasks.component.html',
  styleUrls: ['./completedtasks.component.css']
})
export class CompletedtasksComponent implements OnInit{
  public tasks?: Task[];
  public info?: string;
  public error?: string;
  public taskStatus: string = 'completed';

  constructor(
    private taskDataService: TasksDataService,
    private router: Router) {}

  public getUserTasks(): void {
    this.taskDataService.getUserTasks()
      .then(receivedTasks => {
          this.tasks = receivedTasks.filter(task => task.status === 'completed');
      })
      .catch(error => {
          alert(error.statusText)
          return this.router.navigate(['/']);
      });
  }
  ngOnInit() {
    this.getUserTasks();
  }

  markAsDone(_id: string | undefined, $event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
    this.taskDataService.markTaskAsDone(_id)
      .then(response => {
        this.tasks = this.tasks?.filter(task => task._id !== _id);
        this.info = response.status
      })
      .catch(error => this.error = error.message)
  }
}
