import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletedtasksComponent } from './completedtasks.component';

describe('CompletedtasksComponent', () => {
  let component: CompletedtasksComponent;
  let fixture: ComponentFixture<CompletedtasksComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CompletedtasksComponent]
    });
    fixture = TestBed.createComponent(CompletedtasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
