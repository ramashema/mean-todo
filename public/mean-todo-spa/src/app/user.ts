export class User {
  email: string | undefined;
  firstname: string | undefined;
  middlename: string | undefined;
  surname: string | undefined
}
