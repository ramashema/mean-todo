import {Inject, Injectable} from '@angular/core';
import {TasksDataService} from "./tasks-data.service";
import {BROWSER_STORAGE} from "./storage";
import {User} from "./user";
import {response} from "express";
import {Authresponse} from "./authresponse";
import {parse} from "dotenv";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(
    @Inject(BROWSER_STORAGE) private storage: Storage,
    private taskDataService: TasksDataService,) { }

  public saveToken(token: string): void {
    this.storage.setItem('mean-to-do-token', token);
  }

  public getToken(): string | null {
    return this.storage.getItem('mean-to-do-token')
  }

  public login(user: User): Promise<any>{
      return this.taskDataService.login(user)
      .then( (authRes: Authresponse) => this.saveToken(authRes.token))
      .catch(error => {
        throw error;
      });
  }

  public isLogged(): boolean{
    const token = this.getToken();
    if(token){
      const payload = JSON.parse(atob(token.split('.')[1]));
      return payload.exp > (Date.now() / 1000)
    } else {
      return false
    }
  }

  public logout(): void{
    return this.storage.removeItem('mean-to-do-token');
  }

  public getCurrentUser(): User{
    if(this.isLogged()){
      const token = this.getToken();
      //@ts-ignore
      const { email, firstname, middlename, surname } = JSON.parse(atob(token?.split('.')[1]));
      return { email, firstname, middlename, surname } as User;
    }
    throw 'Your not logged in!'
  }
}
