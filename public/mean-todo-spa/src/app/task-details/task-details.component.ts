import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router, ParamMap} from "@angular/router";
import {of, switchMap} from "rxjs";
import {TasksDataService} from "../tasks-data.service";
import {Task} from "../task";

@Component({
  selector: 'app-task-details',
  templateUrl: './task-details.component.html',
  styleUrls: ['./task-details.component.css']
})
export class TaskDetailsComponent implements OnInit{

  public task: Task | null = {};
  public info: string = "";
  public error: string = "";
  public status: string | undefined = ""

  constructor(private activatedRoute: ActivatedRoute,
              private tasksDataService: TasksDataService,
              private router: Router
              ) {}


  public markAsDone(task?: Task | null) {
    this.tasksDataService.markTaskAsDone(task?._id)
      .then(response => {
        this.info = response.message
        this.status = response.message
      })
      .catch(error => {
        this.error = error.error.message
      });
  }

  public ngOnInit() {
    this.activatedRoute.paramMap.pipe( switchMap((params: ParamMap) => {
      let id: string | null = params.get('id');
      return this.tasksDataService.getSingleTask(id)
    })
    ).subscribe( task => {
      this.task = task
    });
  }

  public seeAllTasks() {
    return this.router.navigateByUrl('/home');
  }
}
