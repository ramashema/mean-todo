import {Component, Input} from '@angular/core';
import {AuthenticationService} from "../authentication.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  @Input() content: any;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) {}

  public doLogout(): void {
    this.authenticationService.logout();
    this.router.navigateByUrl('/');
  }
}
