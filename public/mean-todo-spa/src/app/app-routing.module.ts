import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {LoginComponent} from "./login/login.component";
import {TaskDetailsComponent} from "./task-details/task-details.component";
import {UpcomingtasksComponent} from "./upcomingtasks/upcomingtasks.component";
import {OnprogresstasksComponent} from "./onprogresstasks/onprogresstasks.component";
import {CompletedtasksComponent} from "./completedtasks/completedtasks.component";
import {CancelledtasksComponent} from "./cancelledtasks/cancelledtasks.component";



const taskListRoutes: Routes = [
  { path: '', component: UpcomingtasksComponent },
  { path: 'on-progress', component: OnprogresstasksComponent },
  { path: 'completed', component: CompletedtasksComponent },
  { path: 'cancelled', component: CancelledtasksComponent },
]

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'home', component: HomeComponent, children: taskListRoutes },
  { path: 'task/:id', component: TaskDetailsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
