import { Injectable } from '@angular/core';
import {TasksDataService} from "./tasks-data.service";
import {Task} from "./task";

@Injectable({
  providedIn: 'root'
})
export class ChangeTaskStatusService {

  constructor(private taskDataService: TasksDataService) { }

  public updateTaskStatus(_id: string | undefined): Promise<any> {
    return this.taskDataService.markTaskAsDone(_id)
      .then( response => response)
      .catch(error => {
        throw error
    });
  }
}
