import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit{

  public content = {
    header : 'Tasks List'
  }

  public info: string = '';
  public error: string = '';


  constructor() {}

  public ngOnInit() {
  }
}
