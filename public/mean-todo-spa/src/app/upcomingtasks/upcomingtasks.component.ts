import {Component, Input, OnInit} from '@angular/core';
import {Task} from "../task";
import {TasksDataService} from "../tasks-data.service";
import {Router} from "@angular/router";
import {ChangeTaskStatusService} from "../change-task-status.service";
import {response} from "express";

@Component({
  selector: 'app-upcomingtasks',
  templateUrl: './upcomingtasks.component.html',
  styleUrls: ['./upcomingtasks.component.css']
})
export class UpcomingtasksComponent implements OnInit{

  @Input() addedTask?: Task
  public tasks?: Task[];
  public info?: string;
  public error?: string;
  public taskStatus: string = 'upcoming'

  public showTaskForm: boolean = false;

  public task = {
    title: '',
    body: ''
  }

  constructor(
    private taskDataService: TasksDataService,
    private router: Router,
    private changeTaskStatus: ChangeTaskStatusService) {}

  public getUserTasks(): void {
    this.taskDataService.getUserTasks()
      .then(receivedTasks => {
          this.tasks = receivedTasks.filter(task => task.status === 'upcoming');
      })
      .catch(error => {
          return this.router.navigate(['/']);
      });
  }


  ngOnInit() {
    this.getUserTasks();
  }

  public showAddTaskForm() {
    this.showTaskForm = !this.showTaskForm;
  }

  public onAddTaskFormSubmit(): void {
    if(!this.task.title || !this.task.body){
      this.error = 'All fields are required for task creation';
      this.info = ''
    } else {
      this.addNewTask()
      this.info = 'Success'
      this.error = ''
      this.task.title = ''
      this.task.body = ''
    }
  }

  public addNewTask(): void {
    this.taskDataService.createNewTask(this.task)
      .then(response => {
        this.showTaskForm = false;
        this.tasks?.unshift(response.newSavedTask);
      })
      .catch(error => this.error = error.message)
  }


  public updateTaskStatus(_id: string | undefined, $event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault()
    this.changeTaskStatus.updateTaskStatus(_id)
      .then( response => {
        this.tasks = this.tasks?.filter( task => task._id !== _id );
        this.info = response.message
      }).catch( error => {
        this.error = error.message
    });
  }
}
