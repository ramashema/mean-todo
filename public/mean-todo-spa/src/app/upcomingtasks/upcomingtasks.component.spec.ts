import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpcomingtasksComponent } from './upcomingtasks.component';

describe('UpcomingtasksComponent', () => {
  let component: UpcomingtasksComponent;
  let fixture: ComponentFixture<UpcomingtasksComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UpcomingtasksComponent]
    });
    fixture = TestBed.createComponent(UpcomingtasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
