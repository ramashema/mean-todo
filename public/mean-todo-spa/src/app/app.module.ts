import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from "@angular/common/http";
import { HomeComponent } from './home/home.component';
import { FrameworkComponent } from './framework/framework.component';
import { HeaderComponent } from './header/header.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { LoginComponent } from './login/login.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NgOptimizedImage} from "@angular/common";
import { TaskDetailsComponent } from './task-details/task-details.component';
import { UpcomingtasksComponent } from './upcomingtasks/upcomingtasks.component';
import { OnprogresstasksComponent } from './onprogresstasks/onprogresstasks.component';
import { CompletedtasksComponent } from './completedtasks/completedtasks.component';
import { CancelledtasksComponent } from './cancelledtasks/cancelledtasks.component';
import { AddnewtaskComponent } from './addnewtask/addnewtask.component';

@NgModule({
  declarations: [
    LoginComponent,
    FrameworkComponent,
    HomeComponent,
    HeaderComponent,
    SideBarComponent,
    TaskDetailsComponent,
    UpcomingtasksComponent,
    OnprogresstasksComponent,
    CompletedtasksComponent,
    CancelledtasksComponent,
    AddnewtaskComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgOptimizedImage
  ],
  providers: [],
  bootstrap: [FrameworkComponent]
})
export class AppModule { }
