import {Component, OnInit} from '@angular/core';
import {Task} from "../task";
import {TasksDataService} from "../tasks-data.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-cancelledtasks',
  templateUrl: './cancelledtasks.component.html',
  styleUrls: ['./cancelledtasks.component.css']
})
export class CancelledtasksComponent implements OnInit{

  public tasks?: Task[];
  public info?: string;
  public error?: string;

  constructor(
    private taskDataService: TasksDataService,
    private router: Router) {}

  public getUserTasks(): void {
    this.taskDataService.getUserTasks()
      .then(receivedTasks => {
          this.tasks = receivedTasks.filter(task => task.status === 'cancelled');
      })
      .catch(error => {
          alert(error.statusText)
          return this.router.navigate(['/']);
      });
  }
  ngOnInit() {
    this.getUserTasks();
  }
}
