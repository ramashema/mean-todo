import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelledtasksComponent } from './cancelledtasks.component';

describe('CancelledtasksComponent', () => {
  let component: CancelledtasksComponent;
  let fixture: ComponentFixture<CancelledtasksComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CancelledtasksComponent]
    });
    fixture = TestBed.createComponent(CancelledtasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
