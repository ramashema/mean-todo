import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from "../authentication.service";
import {Router} from "@angular/router";
import {error} from "@angular/compiler-cli/src/transformers/util";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{

  public credentials: any = {
    email: '',
    password: ''
  }

  public error: string = ''

  public formErrors: string = "";

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) {}

  private formIsValid(): boolean{
    return !!(this.credentials.email && this.credentials.password);
  }

  public onLoginSubmit() {
    if(this.formIsValid()){
      this.doLogin();
    }else {
      this.formErrors = 'All field are required for authentication'
    }
  }

  private doLogin(): void {
    this.authenticationService.login(this.credentials)
      .then(() => this.router.navigateByUrl('/home'))
      .catch(error => {
        console.log(error)
        this.formErrors = JSON.stringify(error.error.info)
      });
  }

  ngOnInit() {
  }
}


