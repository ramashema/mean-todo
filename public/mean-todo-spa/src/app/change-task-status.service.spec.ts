import { TestBed } from '@angular/core/testing';

import { ChangeTaskStatusService } from './change-task-status.service';

describe('ChangeTaskStatusService', () => {
  let service: ChangeTaskStatusService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChangeTaskStatusService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
