import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Task} from "./task";
import {User} from "./user";
import {Authresponse} from "./authresponse";
import {BROWSER_STORAGE} from "./storage";
import {response} from "express";
import {error} from "@angular/compiler-cli/src/transformers/util";


@Injectable({
  providedIn: 'root'
})
export class TasksDataService {

  private APIURL: string = 'http://localhost:4000/api'
  constructor(
    private http: HttpClient,
    @Inject(BROWSER_STORAGE) private storage: Storage
  ) { }

  public getAllTasks(): Promise<Task[]> {
    const url = `${this.APIURL}/tasks`;
    return this.http.get(url)
      .toPromise()
      .then(response => response as Task[])
      .catch( error => {
        console.log(error.message);
        throw error
      })
  }

  /**
   * Get user tasks
   * @return Promise
   */
  public getUserTasks(): Promise<Task[]> {
    const url: string = `${this.APIURL}/tasks/user-tasks`;
    const httpRequestOptions = {
      headers: new HttpHeaders({
        'Authorization': `Bearer ${this.storage.getItem('mean-to-do-token')}`
      })
    }
    return this.http.get(url, httpRequestOptions).toPromise()
      .then(response => response as Task[])
      .catch(error => {
        throw error
      });
  }

  public getSingleTask(id: string | null): Promise<Task> {

    const url: string = `${this.APIURL}/tasks/task/${id}`;
    const httpRequestOptions = {
      headers: new HttpHeaders({
        'Authorization': `Bearer ${this.storage.getItem('mean-to-do-token')}`
      }),
    }

    return this.http.get(url, httpRequestOptions).toPromise()
      .then( response => response as Task )
      .catch( error => {
        throw error
      });
  }

  public markTaskAsDone(_id: string | undefined): Promise<any>{
    const url: string = `${this.APIURL}/tasks/mark-as-done/${_id}`;
    const httpRequestOptions = {
      headers: new HttpHeaders({
        'Authorization': `Bearer ${this.storage.getItem('mean-to-do-token')}`
      })
    }
    return this.http.get(url, httpRequestOptions).toPromise()
      .then(response => response)
      .catch(error => {
        throw error
      });
  }

  /**
   * Create a new task
   * @param task
   */
  public createNewTask(task: Task): Promise<any>{
    const url: string = `${this.APIURL}/tasks/add-task`;
    const httpRequestOptions = {
      headers: new HttpHeaders({
        'Authorization': `Bearer ${this.storage.getItem('mean-to-do-token')}`
      })
    }
    return this.http.post(url, task, httpRequestOptions).toPromise()
      .then(response => response)
      .catch(error => {
        throw error
      });
  }

  /**
   * Login request to the API SERVER
   * @param user
   * @return Promise
   */
  public login(user: User): Promise<Authresponse> {
    const url: string = `${this.APIURL}/user/login`;
    console.log(url)
    return this.http
      .post(url, user)
      .toPromise()
      .then(response => response as Authresponse)
      .catch(error => {
        throw error
      });
  }
}
