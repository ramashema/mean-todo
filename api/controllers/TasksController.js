const TaskSchema = require('../models/task');
const mongoose = require("mongoose");
const {response} = require("express");
const {resetWatchers} = require("nodemon/lib/monitor/watch");

const Task = mongoose.model('Task', TaskSchema);

/***
 * List all the tasks from the database
 * @param request
 * @param response
 * @return response
 * @constructor
 */
const ListTasks = (request, response) => {
    Task.find().exec()
        .then( tasks => {
            if(tasks.length === 0){
                return response.status(404).json({
                    message: "Not tasks found in the database!"
                });
            }
            return  response.status(200).json(tasks);
        }).catch( error => {
            return response.status(500).json({
                message: error.message
            });
        });
}

/***
 * Get the tasks list from a single user
 * @param request
 * @param response
 * @return response
 * @constructor
 */
const GetUserTasks = (request, response) => {
    if(request.auth && request.auth.email){
        Task.find({ user: request.auth.email }).exec()
            .then( tasks => {
                if(tasks.length === 0){
                    return response.status(404).json({
                        message: "No task found!"
                    });
                }
                return response.status(200).json(tasks);
            }).catch(error => {
                return response.status(400).json({
                    message: error.message
                });
            });
    } else {
        return response.status(401).json({
            status: 'error',
            message: 'unauthorized'
        });
    }
}

/***
 * Create new task
 * @param request
 * @param response
 * @return response
 * @constructor
 */
const CreateTask = (request, response) => {

    if(!request.auth || !request.auth.email){
        return  response.status(401).json({
            status: 'error',
            message: 'Unauthorized action'
        });
    }

    const title = request.body.title;
    const body = request.body.body;
    const username = request.auth.email;

    if (!title || !body || !username){
        return response.status(401).json({
            status: "error",
            message: "All fields are required to create new task"
        });
    }

    const newTask =  new Task({
            title: title,
            body: body,
            user: username
        });

    newTask.save().then( savedTask => {
        return response.status(200).json({
            message: "New task has been saved!",
            newSavedTask: savedTask
        });
    }).catch( error => {
        return response.status(500).json({
            message: error.message
        });
    } );
}

/***
 * Get a single user task from a database
 * @param request
 * @param response
 * @constructor
 * @return response
 */
const GetSingleTask = (request, response) => {

    if(request.auth && request.auth.email){

        const id = request.params.id;
        const email = request.auth.email;

        Task.findOne({user: email, _id: id}).exec()
            .then( task => {
                if(!task){
                    return response.status(404).json({
                        message: "Something went wrong, requested task can not be found!"
                    });
                }
                return response.status(200).json(task);
            } ).catch( error => {
            return response.status(500).json({
                message: error.message
            });
        });
    } else {
        return response.status(401).json({
            status: 'error',
            message: 'Unauthorized access, please login to access this page'
        });
    }
}

/***
 * Delete a single task from a database
 * @param request
 * @param response
 * @constructor
 * @return response
 */
const DeleteTask = (request, response) => {
    const id = request.params.id;
    const username = request.params.username;

    Task.findOneAndDelete({user: username, _id: id}).then( task => {
        if(!task){
            return response.status(404).json({
                message: "Something went wrong, task can not be deleted!"
            });
        }

        return response.status(200).json({
            status: "success",
            deletedTask: task
        });
    }).catch( error => {
        return response.status(200).json({
            message: error.message
        });
    });
}

function saveTask(task, response) {
    task.save().then(() => {
        return response.status(200).json({
            status: 'success',
            message: task.status
        });
    }).catch(error => {
        return response.status(400).json({
            status: 'error',
            message: error.error.message
        });
    });
}

const MarkAsDone = (request, response) => {
    if(request.auth && request.auth.email){
        if(request.params.id){
            Task.findById(request.params.id)
                .then(task => {
                    if(!task){
                        throw "Something went wrong, we can't find the task";
                    }

                    if(task.user !== request.auth.email){
                        throw "Unauthorized action";
                    }

                    /*
                    Improved version of the function to handle multiple scenarios
                     */
                    switch (task.status){
                        case 'on-progress':
                            task.status = 'completed';
                            saveTask(task, response);
                            break;

                        case 'completed':
                            task.status = 'on-progress';
                            saveTask(task, response);
                            break;

                        case 'upcoming':
                            task.status = 'on-progress';
                            saveTask(task, response);
                            break;
                    }
                }).catch(error => {
                    return response.status(400).json({
                        status: 'error',
                        message: error.error.message
                    });
            });
        }
    }
}

module.exports = {
    ListTasks,
    CreateTask,
    DeleteTask,
    GetSingleTask,
    GetUserTasks,
    MarkAsDone
}