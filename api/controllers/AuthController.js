const mongoose = require("mongoose");
const passport = require('passport');

const UserSchema = require('../models/user');

const User = mongoose.model('User', UserSchema);
const pattern = /^[\w.-]+@[a-zA-Z_-]+?(?:\.[a-zA-Z]{2,})+$/
const Register = (request, response) => {
    const firstname = request.body.firstname;
    const middlename = request.body.middlename;
    const surname = request.body.surname;
    const password = request.body.password;
    const email = request.body.email;


    if(!firstname || !middlename || !surname || !email){
        return response.status(400).json({
            message: 'All fields are required to complete user registration'
        });
    }

    if(password.length < 8){
        return response.status(400).json({
            message: 'The password must be 8 characters or more'
        });
    }

    if(!pattern.test(email)){
        return response.status(400).json({
            message: 'Invalid email address!'
        });
    }

    const user = new User();
    user.firstname = firstname;
    user.middlename = middlename;
    user.surname = surname;
    user.email = email;
    user.setUserPassword(password);

    user.save()
        .then( savedUser => {
        return response.status(200).json({
            token: user.generateJWT()
        });
        })
        .catch( error => {
            return response.status(500).json({
                message: error.message
            });
        });
}

const Login = (request, response) => {
    console.log(request.body)
    const username = request.body.email;
    const password = request.body.password;

    if(!username || !password){
        return response.status(400).json({
            message: 'All fields are required for login'
        });
    }

    if(!pattern.test(username)){
        return response.status(400).json({
            message: 'Username should be a valid email address'
        });
    }

    passport.authenticate('local', {session: false},(error, user, info) => {
        if(error){
            return response.status(401).json({
                error: error.message
            });
        }

        if(user){
            return response.status(200).json({
                token: user.generateJWT()
            });
        }

        if(info){
            return response.status(400).json({
                info: info.message
            });
        }
    })(request, response);

}

module.exports = {
    Register, Login
}