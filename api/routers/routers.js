const express = require('express');
const { expressjwt: jwt } = require('express-jwt')

require('dotenv').config();

const { MarkAsDone, CreateTask, DeleteTask, GetSingleTask, GetUserTasks } = require('../controllers/TasksController');
const { Register, Login } = require('../controllers/AuthController');

const router = express.Router();

const auth = jwt({
    secret: process.env.JWT_SECRET,
    algorithms: ['HS256']
});

//Tasks Paths
// router.get('/tasks', ListTasks);
router.post('/tasks/add-task', auth, CreateTask);
router.get('/tasks/delete-task/:id/:username', auth, DeleteTask);
router.get('/tasks/task/:id', auth, GetSingleTask);
router.get('/tasks/user-tasks', auth, GetUserTasks);
router.get('/tasks/mark-as-done/:id', auth, MarkAsDone);

//Authentication Paths
router.post('/user/registration', Register);
router.post('/user/login', Login);

module.exports = router