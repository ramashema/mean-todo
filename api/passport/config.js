const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const UserSchema = require('../models/user');
const mongoose = require("mongoose");
const User = mongoose.model('User', UserSchema);



passport.use('local', new LocalStrategy({
    usernameField: "email"
}, (username, password, done) => {
    User.findOne({email: username }).exec()
        .then( user => {
            if(!user){
                return done(null, false, {
                    message: 'User is not found!'
                });
            }

            if(!user.validatePassword(password)){
                return done(null, false, {
                    message: 'Incorrect Password!'
                });
            }

            return done(null, user);
        })
        .catch( error => done(null, false, {
            message: error.message
        }));
}));