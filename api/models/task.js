const mongoose = require('mongoose');

const TaskSchema = new mongoose.Schema({
    title: {
        type: String,
        required: [ true, 'Task title is required' ]
    },
    body: {
        type: String,
        required: [ true, 'Task description is required' ]
    },
    status: {
        type: String,
        required:true,
        enum: ['upcoming', 'on-progress', 'completed', 'cancelled'],
        default: 'upcoming'
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },
    completedAt: Date,
    user: {
        type: String,
        default: 'yusuphshema'
    }
});

module.exports = TaskSchema;