const mongoose = require('mongoose');
const crypto = require('crypto');
const  jwt = require('jsonwebtoken');

require('dotenv').config();

const UserSchema = new mongoose.Schema({
    firstname: {
        type: String,
        required: [true, 'First name is required']
    },
    middlename: {
        type: String,
    },
    surname: {
        type: String,
        required: [true, 'Surname is required']
    },
    email: {
        type: String,
        required: [true, 'Email is required'],
        unique: [true, 'Email must be unique']
    },
    salt: String,
    hash: String
});


UserSchema.methods.setUserPassword = function (password) {
    this.salt = crypto.randomBytes(32).toString('hex');
    this.hash = crypto.createHash('sha256').update(this.salt + password).digest('hex');
}

UserSchema.methods.validatePassword = function (password){
    const hash = crypto.createHash('sha256').update(this.salt + password).digest('hex');
    return hash === this.hash;
}

UserSchema.methods.getFullName = function() {
   return this.middlename ? this.firstname + " "+this.middlename+" "+this.surname : this.firstname +" "+this.surname;
}

UserSchema.methods.generateJWT = function() {
    const expire = new Date()
    expire.setDate(expire.getDate() + 7)

    return jwt.sign({
        _id: this._id,
        email: this.email,
        name: this.getFullName(),
        exp: parseInt(expire.getTime() / 1000, 10)
    }, process.env.JWT_SECRET);
}

module.exports = UserSchema;