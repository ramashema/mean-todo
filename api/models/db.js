const mongoose = require('mongoose');

require('dotenv').config()

const dbURL = process.env.DB_URL;

mongoose.connect(dbURL, { dbName: 'mean-todo', useNewUrlParser: true, useUnifiedTopology: true });

mongoose.connection.on('connected', () => {
   console.log(`mean-todo has been connected on ${dbURL}`);
});

mongoose.connection.on('error', () =>  {
    console.error(`the is a problem when connected to the database on ${dbURL}`);
});

const gracefulTermination = (reason, callbackFn) => {
    mongoose.connection.close();
    console.log(`mean-todo has been disconnected from the add due to ${reason}`);
    callbackFn();
}
process.once('SIGUSR2', () => {
    gracefulTermination('NODEMON restart', () =>{
        process.kill(process.pid, 'SIGUSR2');
    });
});

process.on('SIGTERM', () => {
    gracefulTermination('Application Termination', () => {
        process.exit(0);
    });
});